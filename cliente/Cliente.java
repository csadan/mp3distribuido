package cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Cliente {

	private Socket cliente;
	
	private OutputStream cout;
	private InputStream cin;
	private SourceDataLine line;

	private String direccion;
	private Integer puerto;

	private String Nick;
	private String id;

	private String tamano = new String("0");

	private Thread t;
	private Timer time;
	
	private boolean home_modified = false;
	private int position=0;
	
	

	// ############################################################################
	// ################### Metodos Principales ####################################
	// ############################################################################

	public void Conectar(String dir, Integer port , String Nick) throws IOException {
		try {
			this.Nick=Nick;
			
			this.direccion = dir;
			this.puerto = port;
			
			abrirConexion();
			this.id = peticion_id();
			cliente.close();
			
					TimerTask r = new TimerTask() {
						public void run() {
										try {
												cliente = new Socket(direccion, puerto);
												DataInputStream dis = new DataInputStream(cliente.getInputStream());
												PrintStream ps = new PrintStream(cliente.getOutputStream());
												ps.print("isMOD "+id+"\n");
												
												if(dis.readLine().compareTo("OK")==0){
													setHome_modified(true);
												}
												else  setHome_modified(false);
												cliente.close();
												
										}catch (UnknownHostException e) {
											e.printStackTrace();
										}catch (SocketException e) { 
										
										}catch (IOException e) {
											e.printStackTrace();
										}
						}
					};
		
					this.time= new Timer();
					time.scheduleAtFixedRate(r, 0, 2000);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void abrirConexion() throws IOException {

		cliente = new Socket(direccion, puerto);
		cout = cliente.getOutputStream();
		cin = cliente.getInputStream();

	}

	public Vector<String> lista_reproduccion() {
		try {
			System.out.println("+ se abre conexion");
			abrirConexion();
			peticionLista();
			Vector<String> rtd = getLista();
			cliente.close();
			System.out.println("- se cierra conexion");
			return rtd;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void reproducir(String recurso) throws IOException,InterruptedException {

		try {
			
			System.out.println("+ se abre conexion");
			abrirConexion();
			peticionRecurso(recurso);
			reproduce();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void parar() {
		try {
			
			if (t != null) {
				line.close();
				cliente.close();
				t.interrupt();
				System.out.println("- se cierra la conexion");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void desconectar() throws IOException {
		time.cancel();
		parar();
		System.out.println("+ se abre conexion");
		abrirConexion();
		peticionDesconectar();
		cliente.close();
		System.out.println("- se cierra la conexi�n");
	}
	
	public void cambioDirectorio(String dir) throws IOException{
		System.out.println("+ se abre conexion");
		abrirConexion();
		peticionCambioDirectorio(dir);
		cliente.close();
		System.out.println("- se cierra la conexion");
	}
	

	
	
	// ############################################################################
	// ################### Metodos De Peticion ####################################
	// ############################################################################
	
	private void peticionCambioDirectorio(String dir){
		PrintStream ps = new PrintStream(cout);
		ps.print("CHNGDIR "+this.id+" "+dir+"\n");
	}

	private void peticionDesconectar() throws IOException {
		PrintStream ps = new PrintStream(cout);
		ps.print("DISCONECT "+this.id+"\n");
	}

	private void peticionLista() {
		PrintStream ps = new PrintStream(cout);
		ps.print("GETLISTDIR "+this.id+"\n");
	}

	private void peticionRecurso(String recurso) {
		PrintStream ps = new PrintStream(cout);
		ps.print("GETFILE "+this.id +"\n");
		ps.print(recurso + "\n");
	}

	private String peticion_id() throws IOException {
		
		 PrintStream ps = new PrintStream(cout);
		 DataInputStream dis = new DataInputStream(cin);
		 
		 ps.print("CONNECT "+this.Nick+"\n");
		 String respuesta = dis.readLine();
		
		 return respuesta.split(" ")[1];
	}

	
	
	
	
	// ############################################################################
	// ################### Metodos De Recepcion
	// ###################################
	// ############################################################################

	
	public void refrescado(){
		try {
			
			cliente = new Socket(direccion, 13133);
			PrintStream ps = new PrintStream(cliente.getOutputStream());
			ps.print("REFRESH "+this.id+"\n");
			cliente.close();	
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	private Vector<String> getLista() throws IOException {

		Vector<String> rtd = new Vector<String>();
		DataInputStream dis = new DataInputStream(cin);
		String linea = dis.readLine();
		String[] lista = linea.split(" ");
		
		if (lista[1].compareTo("OK") == 0) {
			
			String archivos = dis.readLine();
			String[] files = archivos.split("/");
			int i = 0;
			while (i < files.length) {
				System.out.println(files[i]);
				rtd.add(files[i]);
				i++;
			}		
		}
		
		return rtd;
	}

	private void reproduce() throws IOException {
		try {
			
			
			DataInputStream dis = new DataInputStream(cin);
			String linea = dis.readLine();
			System.out.println("----------"+linea);
			
			if (linea.split(" ")[1].compareTo("OK") == 0) {

					this.tamano = linea.split(" ")[2];
	
					AudioInputStream ais = AudioSystem.getAudioInputStream(cin);
	
					final AudioFormat format = getAudioFormat();                                   // Este m�todo est� m�s abajo
					final AudioInputStream din = AudioSystem.getAudioInputStream(format, ais);
					DataLine.Info info = new DataLine.Info(SourceDataLine.class,format);
					this.line = (SourceDataLine) AudioSystem.getLine(info);
	
					if (line != null) {
						t = new Thread(new Runnable() {
							public void run() {
								try {
									line.open(format);
									line.start();
									byte[] data = new byte[4096];
									int leidos;
	
									while ((leidos = din.read(data)) != -1 && !Thread.currentThread().isInterrupted()) {
										line.write(data, 0, leidos);
										setPosition(line.getFramePosition());
									}
									if(leidos==-1){
										line.close();
										cliente.close();
									}
								} catch (LineUnavailableException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								} 
							}
						});
						t.start();
					}
				
			}
			
			else{
				System.out.println(linea);
			}

		} catch (LineUnavailableException ex) {

			ex.printStackTrace();

		} catch (UnsupportedAudioFileException ex) {

			ex.printStackTrace();

		} catch (IOException ex) {

			ex.printStackTrace();

		}

	}

	private AudioFormat getAudioFormat() {

		AudioFormat.Encoding encoding = new AudioFormat.Encoding("PCM_SIGNED");

		float sampleRate = 44100.0F; // 8000,11025,16000,22050,44100
		int sampleSizeInBits = 16; // 8,16
		int channels = 2; // 1,2
		boolean bigEndian = false; // true,false

		AudioFormat format = new AudioFormat(encoding, sampleRate,sampleSizeInBits, channels, channels * 2, sampleRate, bigEndian);

		return format;
	}

	
	
	
	
	// ############################################################################
	// ################### Metodos Accesores
	// ######################################
	// ############################################################################

	public String getTamano() {
		return tamano;
	}

	public boolean getHome_modified() {
		return home_modified;
	}

	private void setHome_modified(boolean home_modified) {
		this.home_modified = home_modified;
	}

	public Long getPosition() {
		return line.getMicrosecondPosition();
	}

	private void setPosition(int position) {
		this.position = position;
	}


	
}