package Servidor;

import java.util.Scanner;

public class PrincipalServidor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Servidor s = new Servidor(args[0], Integer.parseInt(args[1]));
		Thread t = new Thread(s);
		t.start();
		
		Scanner entrada = new Scanner(System.in);
		String op ="";
		while(true){
			System.out.println("\n\n<<<<<<<<<<<<   Menu >>>>>>>>>>");
			System.out.println("1. Ver Clientes");
			System.out.println("2. Eliminar Cliente");
			System.out.println("Introducir opcion: ");
			op = entrada.nextLine();
			if(op.compareTo("1")==0){
				System.out.println(s.getClientes());
			}
			else if(op.compareTo("2")==0){
				System.out.println("introducir id: ");
				System.out.println(s.desconectar(entrada.nextLine()));
			}
			else{
				System.out.println("Opcion incorrecta");
			}
			
		}
		
	}

}
