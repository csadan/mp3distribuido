package Servidor;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;


public class Servidor implements Runnable{
	
	private File f;
	private String dir; 
	private int puerto;
	
	//almacenamiento datos de sesion
	private Hashtable<String, String> clientes; 		//nick e id
	private Hashtable<String, Long> cliente_mod; 		//id y fecha de la ultima modificacion del directorio
	private Hashtable<String, String> clientes_dir; 	//id y directorio del cliente
	
	
	public Servidor(String h,int p){
		this.dir=h;
		this.puerto=p;

		clientes  = new Hashtable<String, String>();
		cliente_mod = new Hashtable<String, Long>();
		clientes_dir= new Hashtable<String, String>();

		f= new File(dir);
	
	}
	public void iniciar(){
		try {
			iniciarServidorMp3();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Boolean desconectar(String id){
		if(clientes.containsKey(id)){
			clientes.remove(id);
			cliente_mod.remove(id);
			clientes_dir.remove(id);
			return true;
		}
		else{
			return false;
		}

	}
	
	public Set<String> getClientes(){
		return clientes.keySet();
	}
	
	
	private void iniciarServidorMp3() throws IOException {				
			ServerSocket ss;
			ss = new ServerSocket(this.puerto);
			ExecutorService pool= Executors.newCachedThreadPool();;
			while(!Thread.currentThread().isInterrupted()){
				final Socket cliente= ss.accept();
				Runnable r = new Runnable() {
							public void run() {
								try {
									
									DataInputStream cin;
									cin = new DataInputStream(cliente.getInputStream());
									PrintStream out= new PrintStream(cliente.getOutputStream());
									
									String linea = cin.readLine();
								
											String [] peticion= linea.split(" ");
											String metodo= peticion[0];
											
											if(metodo.compareTo("GETLISTDIR")==0){
												if(clientes.containsKey(peticion[1]))  ProcesarGETLISTDIR(out,peticion[1]);
												else out.print("ID incorrecto\n");
											}
														
											else if(metodo.compareTo("GETFILE")==0){
													if(clientes.containsKey(peticion[1]))  ProcesarGETFILE(cin.readLine(), out, peticion[1]);
													else {
														cin.readLine();
														out.print("ID incorrecto\n");
													}
											}
											
											else if(metodo.compareTo("CONNECT")==0){
												    ProcesarCONNECT(peticion[1],out);	
											}
											
											else if(metodo.compareTo("DISCONECT")==0){
												if(clientes.containsKey(peticion[1])){
													clientes.remove(peticion[1]);
													cliente_mod.remove(peticion[1]);
													clientes_dir.remove(peticion[1]);
												}
												else out.print("ID incorrecto\n");	
											}
											
											
											else if(metodo.compareTo("isMOD")==0){
												if(clientes.containsKey(peticion[1])){	
												
														f= new File(clientes_dir.get(peticion[1]));	
																												
														if(f.lastModified()!=cliente_mod.get(peticion[1])){
															PrintStream ps = new PrintStream(out);
															ps.print("OK\n");
														}																
														else{
																PrintStream ps = new PrintStream(out);
															    ps.print("NO\n");
														}
												}else{
													out.print("ID incorrecto\n");
												}
											}
											
											else if(metodo.compareTo("REFRESH")==0){
												cliente_mod.put(peticion[1],f.lastModified());
											}
											
											
											else if(metodo.compareTo("CHNGDIR")==0){
												if(clientes.containsKey(peticion[1])){
													ProcesarCHNGDIR(out,peticion[2],peticion[1]);
												}else{
													out.print("ID incorrecto\n");
												}
											}
									
												
								}catch (IOException e) {
									e.printStackTrace();
								}
							}
					};
				pool.execute(r);
			}
			pool.shutdown();
			ss.close();
			
	}
	
	private void ProcesarCHNGDIR(PrintStream out,String dir,String id) {
		if(dir.equals("HOME")){
			clientes_dir.remove(id);
			clientes_dir.put(id, this.dir);
			ProcesarGETLISTDIR(out,id);
		}
		else{
				f= new File(clientes_dir.get(id)+"/"+dir);
				if(f.isDirectory()){
					clientes_dir.remove(id);
					clientes_dir.put(id, f.getAbsolutePath().replace("\\", "/"));
					ProcesarGETLISTDIR(out,id);
				}
		}
	}
	
	private void ProcesarCONNECT(String address,OutputStream out) {
		PrintStream ps = new PrintStream(out);
		Random a = new Random();
		Integer id=a.nextInt();
		String sid = new String(id.toString());

		ps.print("ID "+id+"\n");
		clientes.put(sid, address);
		cliente_mod.put(sid,f.lastModified());
		clientes_dir.put(sid, dir);
		
	}

	private void ProcesarGETFILE(String peticion,OutputStream out,String id) throws IOException {

				try {
					File f = new File(clientes_dir.get(id) + peticion);
					if (f.exists() && f.isFile()) {
							FileInputStream file= new FileInputStream(f);
							
							 AudioFileFormat baseFileFormat = AudioSystem.getAudioFileFormat(f);
					         Map properties = baseFileFormat.properties();
					         String key_duration = "duration";
					         Long duration = (Long) properties.get(key_duration);
							
							PrintStream ps = new PrintStream(out);
							ps.println("ITEM OK " + duration +"\n");
							
							byte[] buffer = new byte[1024];
							Integer leidos;
							while ((leidos = file.read(buffer)) != -1) {
								out.write(buffer, 0, leidos);
							}
							
					} else {
						PrintStream dos = new PrintStream(out);
						dos.print("ITEM 1");
					}
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedAudioFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

	private void ProcesarGETLISTDIR(PrintStream out,String id) {
			f= new File(clientes_dir.get(id));
			File[]list = f.listFiles();
			int i=0;
			if(list!=null){
				StringBuffer respuesta = new StringBuffer("LIST OK\n");
				while(i<list.length){
					if(list[i].isFile() && list[i].getName().substring(list[i].getName().lastIndexOf(".")+1, list[i].getName().length()).equals("mp3")) respuesta.append(list[i].getName()+"/");
					else if(list[i].isDirectory()) respuesta.append("<Directorio> "+list[i].getName()+"/");
					
					i++;
				}
				out.println(respuesta);
			}
			else{
				out.print("LIST 1\n");
			}
	}
	@Override
	public void run() {
		iniciar();
		
	}

}
