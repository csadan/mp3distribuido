package GUI;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Component;

import javax.swing.DefaultListModel;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JPopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;


import cliente.Cliente;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import java.awt.ComponentOrientation;


public class InterfazGrafica extends JFrame {
	private JPanel contentPane;
	private JTextField TFservidor;
	private Cliente c;
	private JList ListCanciones;
	private int selectedItem=0;
	private DefaultListModel dlm;
	private JButton BTplay;
	public JSlider slider;
	private JTextField tfNick;
	private JButton btnConectar;
	private JButton btnStp;
	private JButton btnDesconectar;
	private JTextField tfpuerto;
	private JLabel lblmodified;
	private JScrollPane jsp;
	private JButton btnVolverAOrigen;
	

	public InterfazGrafica(final Cliente c) {
		setResizable(false);
		
		//Slider
		slider= new JSlider();
		
		//Cliente
		this.c= c;
		
		//Lista de Canciones
		ListCanciones = new JList();
		ListCanciones.setValueIsAdjusting(true);
		ListCanciones.setVisibleRowCount(5);
		ListCanciones.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		ListCanciones.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		ListCanciones.setSelectedIndex(1);
		ListCanciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		//DefaultListModel y JScrollPane
		dlm = new DefaultListModel();
		jsp = new JScrollPane(ListCanciones);
		
		
		setTitle("Reproductor Musica Remoto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 562);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		contentPane = new JPanel();
		contentPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		//Boton Stop
		btnStp = new JButton("Stop");
		btnStp.setEnabled(false);
		btnStp.setToolTipText("Para la canci\u00F3n que se escucha actualmente y la inicializa\r\n");
		btnStp.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if (btnStp.isEnabled()) {
					c.parar();
				}
			}
		});
		
		//Boton play
		BTplay = new JButton("Play");
		BTplay.setToolTipText("Reproducir canción seleccionada");
		BTplay.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				clickPlay();
			
			}
		});
		
		
		JLabel lblServidor = new JLabel("Servidor:");
		
		TFservidor = new JTextField();
		TFservidor.setText("localhost");
		TFservidor.setColumns(10);
		
		
		//Boton de conectar
		btnConectar = new JButton("Conectar");
		btnConectar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				try {
					Conectar();
					btnConectar.setVisible(false);
					btnDesconectar.setVisible(true);
					TFservidor.setEnabled(false);
					tfpuerto.setEnabled(false);
					ListCanciones.setSelectedIndex(0);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		
		//Boton de refrescar
		JButton btnRefrescarCanciones = new JButton("Refrescar Canciones");
		btnRefrescarCanciones.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
					listaCanciones();
					c.refrescado();
					lblmodified.setVisible(false);
			}
		});
		
		JLabel lbNick = new JLabel("Nick:");
		
		//Boton de desconectar
		btnDesconectar = new JButton("Desconectar");
		btnDesconectar.setVisible(false);		
		btnDesconectar.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				try {
					btnDesconectar.setVisible(false);
					btnConectar.setVisible(true);
					TFservidor.setEnabled(true);
					BTplay.setEnabled(true);
					tfpuerto.setEnabled(true);
					dlm.clear();
					c.parar();
					c.desconectar();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		

		//información del puerto
		JLabel lblPuerto = new JLabel("Puerto: ");
		tfpuerto = new JTextField();
		tfpuerto.setText("13133");
		tfpuerto.setColumns(10);
		
		//modificacion de la lista de canciones
		lblmodified = new JLabel("La lista de canciones se encuentra desactualizada, por favor, refrescar canciones");
		lblmodified.setVisible(false);
		
		//boton izquierda
		JButton btnIzquierda = new JButton("<<");
		btnIzquierda.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
						setSelectedItem(getSelectedItem()-1);
						ListCanciones.setSelectedIndex(getSelectedItem());
						clickPlay();
				
			}
		});
		
		
		//boton derecha
		JButton btnDerecha = new JButton(">>");
		btnDerecha.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				
					setSelectedItem(getSelectedItem()+1);
					ListCanciones.setSelectedIndex(getSelectedItem());
					clickPlay();
			}
		});
		
		
		//datos Nick
		tfNick = new JTextField();
		tfNick.setText("anonimo");
		tfNick.setColumns(10);
		
		
		//boton cambio de directorio
		JButton btnCHNGDIR = new JButton("Cambiar Directorio");
		btnCHNGDIR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				
						String seleccion = ListCanciones.getSelectedValue().toString();
						if(seleccion.startsWith("<Directorio>")){
							c.cambioDirectorio(seleccion.substring(seleccion.indexOf(">")+2, seleccion.length()));
							listaCanciones();
							c.refrescado();
							lblmodified.setVisible(false);
							
						}
				
				}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				}
			}
		});
		
		//boton volver a directorio home
		btnVolverAOrigen = new JButton("Volver a Origen");
		btnVolverAOrigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					c.cambioDirectorio("HOME");
					listaCanciones();
					c.refrescado();
					lblmodified.setVisible(false);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * */
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnCHNGDIR)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnVolverAOrigen)
							.addGap(281))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnRefrescarCanciones, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addComponent(lblServidor, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(TFservidor, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(lblPuerto, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(tfpuerto, GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE))
										.addComponent(slider, GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
										.addComponent(jsp, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addComponent(btnStp)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(BTplay)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnIzquierda)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnDerecha)))
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createSequentialGroup()
											.addComponent(lbNick, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(tfNick, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnConectar))
										.addComponent(btnDesconectar))
									.addGap(273))
								.addComponent(lblmodified, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblServidor, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
						.addComponent(TFservidor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPuerto, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(tfpuerto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(14)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnConectar)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(lbNick, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(tfNick, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(17)
							.addComponent(btnDesconectar))
						.addComponent(jsp, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnStp)
						.addComponent(BTplay)
						.addComponent(btnIzquierda)
						.addComponent(btnDerecha))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCHNGDIR, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnVolverAOrigen))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnRefrescarCanciones, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblmodified)
					.addGap(85))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	
	public void mostrarInterfaz() throws InterruptedException {
		setVisible(true);
		new Thread(new Runnable() {
			public void run() {
				while(true){
					
					try {
						
						Thread.currentThread().sleep(1000);
						if(c.getHome_modified()) lblmodified.setVisible(true);
						else lblmodified.setVisible(false);
						
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				}
			}
		}).start();		
	}
	
	
	//REPRODUCIR
	private void reproducir() throws InterruptedException{
		try {
			if(!ListCanciones.getSelectedValue().toString().startsWith("<Directorio>")){
				c.reproducir("\\"+ListCanciones.getSelectedValue().toString());
				
				new Thread(new Runnable() {

					public void run() {
						try {
							slider.setMaximum(Integer.parseInt(c.getTamano()));
							slider.setMinimum(0);
							slider.setEnabled(false);
							while(true){
								Thread.currentThread().sleep(1000);
								slider.setValue(Integer.parseInt(c.getPosition().toString()));		
							}
							
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}).start();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	//OBTENER LISTA DE CANCIONES
	private void listaCanciones(){
		
		dlm.clear();
		ListCanciones.setModel(dlm);
		for(String s : c.lista_reproduccion()){
			dlm.addElement(s);
		}	
		ListCanciones.setModel(dlm);
	}

	
	//CONECTAR
	private void Conectar() throws NumberFormatException, IOException {
			c.Conectar(TFservidor.getText(),Integer.parseInt(tfpuerto.getText()),tfNick.getText());
			listaCanciones();
	}

	
	//OBTENER LA CANCION SELECCIONADA
	private int getSelectedItem() {
		return selectedItem;
	}

	
	//SELECCIONAR UNA CANCION DE LA LISTA DE CANCIONES
	private void setSelectedItem(int selectedItem) {
		if(selectedItem>=0 && selectedItem <= dlm.getSize())
			this.selectedItem = selectedItem;
	}
	
	
	//CLICK SOBRE EL BOTON PLAY
	private void clickPlay() {
		try {
			if(btnStp.isEnabled()){
				c.parar();
			}
			
			btnStp.setEnabled(true);
			reproducir();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
